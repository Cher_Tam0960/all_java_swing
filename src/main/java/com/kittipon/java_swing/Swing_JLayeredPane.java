/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.java_swing;

/**
 *
 * @author kitti
 */
import javax.swing.*;
import java.awt.*;

public class Swing_JLayeredPane extends JFrame {

    public Swing_JLayeredPane() {
        super("LayeredPane Example");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(200, 200);
        JLayeredPane pane = getLayeredPane();
        //creating buttons  
        JButton top = new JButton();
        top.setBackground(Color.white);
        top.setBounds(20, 20, 50, 50);
        JButton middle = new JButton();
        middle.setBackground(Color.red);
        middle.setBounds(40, 40, 50, 50);
        JButton bottom = new JButton();
        bottom.setBackground(Color.cyan);
        bottom.setBounds(60, 60, 50, 50);
        //adding buttons on pane  
        pane.add(bottom, new Integer(1));
        pane.add(middle, new Integer(2));
        pane.add(top, new Integer(3));
    }

    public static void main(String[] args) {
        Swing_JLayeredPane panel = new Swing_JLayeredPane();
        panel.setVisible(true);
    }
}
