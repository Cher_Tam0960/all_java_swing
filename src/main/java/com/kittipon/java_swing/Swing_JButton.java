/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.java_swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author kitti
 */
public class Swing_JButton {

    public static void main(String[] args) {
        JFrame f = new JFrame("Button Example");
        final JTextField tf = new JTextField();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tf.setBounds(50, 50, 150, 20);
        JButton b = new JButton("Click Here");
        b.setBounds(50, 100, 95, 30);
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("click!!!");
                tf.setText("Welcome to Javatpoint.");
            }
        });

        JButton Pic = new JButton(new ImageIcon
        ("C:\\Users\\kitti\\Desktop\\39115210-E3C4-4CC3-8D57-5A2176F2C590.jpeg"));
        Pic.setBounds(50, 150, 80, 30);
        Pic.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ex) {
                System.out.println("Picture click!!!");
                tf.setText("Welcome to Javatpoint.");
            }
        });
        f.add(Pic);

        f.add(b);
        f.add(tf);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

}
