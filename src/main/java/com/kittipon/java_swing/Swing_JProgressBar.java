/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.java_swing;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author kitti
 */
public class Swing_JProgressBar extends JFrame {

    JProgressBar jb;
    int i = 0, num = 0;

    Swing_JProgressBar() {
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jb = new JProgressBar(0, 2000);
        jb.setBounds(40, 40, 160, 30);
        jb.setValue(0);
        jb.setStringPainted(true);
        add(jb);
        setSize(250, 150);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            jb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(50);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        Swing_JProgressBar m = new Swing_JProgressBar();
        m.setVisible(true);
        m.iterate();
    }
}
