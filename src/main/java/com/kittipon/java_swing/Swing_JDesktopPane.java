/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.java_swing;

/**
 *
 * @author kitti
 */
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

public class Swing_JDesktopPane extends JFrame {

    public Swing_JDesktopPane() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);

        setTitle("JDesktopPane Example");
        setSize(300, 350);
        setVisible(true);
    }

    public static void main(String args[]) {
        new Swing_JDesktopPane();
    }
}

class CustomDesktopPane extends JDesktopPane {

    int numFrames = 3, x = 30, y = 30;

    public void display(CustomDesktopPane dp) {
        for (int i = 0; i < numFrames; ++i) {
            JInternalFrame jframe = new JInternalFrame("Internal Frame " + i, true, true, true, true);

            jframe.setBounds(x, y, 250, 85);
            Container c1 = jframe.getContentPane();
            c1.add(new JLabel("I love my country"));
            dp.add(jframe);
            jframe.setVisible(true);
            y += 85;
        }
    }
}
